FROM ubuntu:latest

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN yes | unminimize

RUN apt update && \
    apt install -y \
        cmake \
        gdb \
        build-essential \
        git \
        curl wget \
        vim \
        man-db \
        python3-dev && \
    apt clean

#man-db manpages-posix manpages-dev manpages-posix-dev

COPY vimrc.vim /root/.vimrc

RUN git clone https://github.com/VundleVim/Vundle.vim.git /root/.vim/bundle/Vundle.vim && \
    yes '' | vim +PluginInstall +qall && \
    cd /root/.vim/bundle/YouCompleteMe && \
    python3 install.py --clangd-completer

RUN git clone https://github.com/longld/peda.git ~/peda && \
    echo "source ~/peda/peda.py" >> ~/.gdbinit && \
    echo "set disassembly-flavor att" >> ~/.gdbinit
